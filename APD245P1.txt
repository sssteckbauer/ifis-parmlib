*VARIABLE/FIX F                                                         00000101
*APPLICATION ID FFORIG                                                  00000206
*CUSTOMER ID 6956006144                                                 00000302
*TEST/PROD IND P                                                        00000408
*DOCUMENT ID 820                                                        00000500
*BANK ID 043000261                                                      00000600
*NOTIFICATION EMAIL ACT-PRODCONTROL@UCSD.EDU                            00000705
*TRANSACTION CODE 7                                                     00000803
*PAYMENT METHOD ACH                                                     00000900
*PAYMENT FORMAT CTX                                                     00001000
*ORIG BANK QUALIFIER 01                                                 00001100
*ORIG BANK ID 043000261                                                 00001203
*ORIG ACCT QUALFIER DA                                                  00001300
*ORIG ACCT NBR 70842                                                    00001404
*COMPANY ID 6956006144                                                  00001502
*REC BANK QUALIFIER 01                                                  00001603
*BUS FUNC CODE VEN                                                      00001703
*PAYER NAME UNIVERSITY OF CALIFORNIA SAN DIEGO                          00001803
