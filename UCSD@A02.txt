
#Load the audit records for any files sent to or received #
#from account WFBD, userid WFBD004 (ACH data) #
#The audit data must be retrieved in a subsequent session #

audit account(WFBD) userid(WFBD004);

