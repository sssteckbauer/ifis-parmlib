/* REXX ============================================================ */
/*                                                                   */
/* MAILER:  A REXX EXEC wrapper for TXT2PDF and XMITIP to:           */
/* - send a variable number of e-mail attachment files               */
/* - disregard any attachments which the contents is less than a set */
/*   threshold                                                       */
/*                                                                   */
/* COMMAND SYNTAX:                                                   */
/*                                                                   */
/*   MAILER S, A B C D E F G H I J K                                 */
/*                                                                   */
/*   - S = E-mail subject line.  (Do not use quotes)                 */
/*     , = Delimiter for end of Subject line                         */
/*     A = Y/N Send e-mail option                                    */
/*         S = Send the e-mail even if there are no attachment files */
/*         Y = Send the e-mail even if there are no attachment files */
/*         N = If no attachments, do not send e-mail.                */
/*   B-K = Number of lines thresholds for first thru 10th TXT files. */
/*         If number is =>   than the number of lines in the file,   */
/*                           don't  generate a PDF file              */
/*         If number is =0   create a PDF if it contains any data.   */
/*         If number is <    than the number of lines in the file,   */
/*                           generate a PDF file                     */
/*         Not specified     the default is 0.                       */
/*                                                                   */
/*   Input files are preallocated (JCL or TSO ALLOC) with DDNAME     */
/*   TXTxxxxx, xxxxx being any abritrary characters forming a valid  */
/*   DDNAME.  First allocation relates to threshold option B, second */
/*   'C' for a maximum of 10 DDNAMEs. Over 10 and the EXEC will      */
/*   generate an error message and terminate RC=8.                   */
/*                                                                   */
/*   If there are no PDF files and the user sets option A to 'N',    */
/*   don't send an e-mail if no reports, MAILER generates a warning  */
/*   message and terminates RC=4. The user gets no confirmation      */
/*   whether there was nothing to send or a failure causing the      */
/*   output not to reach its destination.                            */
/*                                                                   */
/* ================================================================= */
TRACE N                                /* TRACE I for testing        */
Sep = ','
mail = 'Y'
PDFYN = 'N'
t. = 0
c. = 0
ARG subject (sep) limits
PARSE VAR limits mailopt t.1 t.2 t.3 t.4 t.5 t.6 t.7 t.8 t.9 t.10
IF ( (mailopt = 'Y') | (mailopt = 'N') | (mailopt = 'S') ) THEN
  mail = mailopt
ELSE DO
  SAY "ERROR:  Send option must be Y, N, or S"
  EXIT 12
END
IF (mail = 'S') THEN
    mail = 'Y'
ELSE NOP
DO i = 1 to 10
  if t.i = '' THEN DO
     t.i = 0
  END
END
/* ----------------------------------------------------------------- */
/* Do a LISTALC looking for any DDNAMEs starting with TXT            */
/* ----------------------------------------------------------------- */
x = OUTTRAP('files.')
"LISTALC STATUS"
x = OUTTRAP('OFF')
DDcount  = files.0
txtDD.      = ''
txtCount = 0
DO i = 1 to DDcount
  IF SUBSTR(files.i, 1, 1) = ' ' THEN DO
    IF SUBSTR(files.i, 3, 3) = 'TXT' THEN DO
      txtCount = txtCount + 1
      IF txtCount > 10 THEN DO
        SAY "ERROR - The maximum number of TXT datasets per run is 10"
        EXIT 8
      END
      txtDD.txtCount = SUBSTR(files.i, 3, 8)
      /* ----------------------------------------------------------- */
      /* read the file and obtain the number of lines in the dataset */
      /* ----------------------------------------------------------- */
      "DELSTACK"
      "EXECIO * DISKR" txtDD.txtCount "(FINIS"
        c.txtCount = QUEUED()
    END
  END
END
"DELSTACK"
pdfString = ""
rptString  = ""
/* ----------------------------------------------------------------- */
/* Run the list of candidate attachment DDNAMES and run TXT2PDF      */
/* against them.                                                     */
/* ----------------------------------------------------------------- */
DO i = 1 to 10
  IF ( (txtDD.i \= '') & (c.i > t.i) ) THEN DO
    pdfYN = 'Y'
    pdfDDname = "PDF"i
    pdfDD = "FI("pdfDDname") NEW UNIT(SYSALLDA) SPACE(1 1) ",
            "CYLINDERS LRECL(27990) BLKSIZE(27998) RECFM(V B)"
    "ALLOC" pdfDD

    pdfOPTs = "IN DD:"txtDD.i" OUT DD:PDF"i" CC YES ",
              "COMPRESS 9 CONFIRM YES LPI 8 TM .0 BM .0 LM .5 RM .0 ",
              "ORIENT LANDSCAPE PAPER /BLUEBAR BROWSE Y"
    "%TXT2PDF" pdfOPTs
    /* ------------------------------------------------------------- */
    /* build an attachment list for XMITIP                           */
    /* ------------------------------------------------------------- */
    pdfString = pdfString || pdfDDname || " "
    rptString = rptString || "FILE"i || ".PDF "
  END
END
/* ----------------------------------------------------------------- */
/* Build the options and execute XMITIP                              */
/* ----------------------------------------------------------------- */
IF ( ( pdfYN = 'Y') | ( mail = 'Y') ) THEN DO
  mailOPTs = "* SUBJECT '"subject"' FROM ACT-ProdControl@ucsd.edu ",
             "ADDRESSFILEDD ADDRLIST ",
             "FILEDD ("pdfString") FILENAME ("rptString") MSGT ",
             "'REPORT RUN ON: &DAY &DATE &TIME, by JOB: &JOB\\\\",
             "THIS REPORT IS VIEWABLE WITH ACROBAT READER.\",
             "THE LATEST VERSION CAN BE DOWNLOADED FROM:\",
             "HTTP://WWW.ADOBE.COM/PRODINDEX/ACROBAT",
             "/READSTEP.HTML#READER'"
  "%XMITIP" mailOPTs
END
IF ( ( pdfYN = 'N') & ( mail = 'N') ) THEN DO
  SAY "WARNING: No attachments meeting thresholds were found, and the"
  SAY "         option for suppressing the e-mail was set."
  EXIT 4
END
