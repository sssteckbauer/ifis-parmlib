send
     account(WFBD) userid(WFBD003)  #Wells Fargo San Francisco
     fileid(DD:UCSDOUT)             #send the file from DD UCSDOUT
     mode( )                        #normal mode
     class(wrrec4)                  #class to identify file content
     charge(3)                      #who pays for transmission?
     ack(f)                         #generate all acknowledgments
     verify(y)                      #make sure dest mailbox is valid
     retain(14);                    #retain for 14 days or until recv'd
