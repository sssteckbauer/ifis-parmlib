*UNIVERSITY CODE 01                                                     00010000
*BANK CODE 01                                                           00020000
*ACH ABA 043000261                                                      00030000
*ORIGINATOR ID 3956006144                                               00040000
*FILE ID A                                                              00050000
*RECORD SIZE 094                                                        00060000
*BLOCKING FACTOR 10                                                     00070000
*FORMAT CODE 1                                                          00080000
*ACH NAME MELLON BANK, N.A                                              00090000
*ORIGIN UCSD                                                            00100000
*SERVICE CLASS CODE 200                                                 00110000
*COMPANY NAME U.C. SAN DIEGO                                            00120000
*COMPANY ID 3956006144                                                  00130000
*ENTRY CLASS CODE PPD                                                   00140000
*COMPANY DESCRIPTION UCSD APACH                                         00150000
*COMPANY ID2 5956006144                                                 00160000
*ENTRY CLASS CODE2 CCD                                                  00170000
*STATUS CODE 1                                                          00180000
*DFI ID 04300026                                                        00190000
*COMMIT COUNTER 0025                                                    00200000
