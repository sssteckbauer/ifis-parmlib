receive
     account(WFBD) userid(WFBD003)  #Wells Fargo San Francisco
     fileid(DD:WFBDOUT)             #recv the file to DD WFBDOUT
     allfiles(n)                    #only rcv 1st file meeting criter.
     class(FISHER);                 #class to identify file content
